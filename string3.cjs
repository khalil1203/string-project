let ans = (str)=>{
    if(str == null || str == undefined){
        return "";
    }
    if (typeof str != 'string'){
        return "";
    }

    let month = str.split("/");
    let monthNames = ["January", "February", "March" ,"April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"];
    return (monthNames[Number(month[1])-1]); 

}
module.exports = ans;