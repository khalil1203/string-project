let ans = (ipString)=>{
    
    if(ipString == null || ipString == undefined){
        return [];
    }
    if (typeof ipString != 'string'){
        return [];
    }
    let answer=[];
    let data = ipString.split(".");
    console.log(data);
    for(let index = 0;index < data.length;index++){
        if(isNaN(data[index])){
            return [];
        }
        answer.push(Number(data[index]));
    }
    return answer;
}
module.exports = ans;