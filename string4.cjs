let ans = (obj)=>{
    if(obj == null || obj == undefined){
        return "";
    }
    if (typeof obj != 'object'){
        return "";
    }
    let result = "";
    for(let index in obj){
        let temp = obj[index];
        let tempstr1 = temp.toLowerCase();
        tempstr1 = tempstr1[0].toUpperCase() + tempstr1.slice(1);
        result += tempstr1 +" ";
    }
    return result;
}
module.exports = ans;