let ans = (data)=>{
    if(!Array.isArray(data)){
        return "";
    }
    if(data.length == 0){
        return "";
    }
    if(data == null || data == undefined){
        return "";
    }
    let result = data.toString().replace(/,/g," ");
    return result;
}
module.exports = ans;