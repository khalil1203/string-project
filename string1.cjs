let ans = (stringData)=>{
    if(stringData == null || stringData == undefined){
        return 0;
    }
    if (typeof stringData != 'string'){
        return 0;
    }
    let result = "";
    for(let index = 0;index<stringData.length;index++){
        let temp = stringData[index];
        if(temp == " "){
            return 0;
        }
        if(!isNaN(temp) || temp == "-" || temp == "."){
            result += temp;
        }
        else{
            continue;
        }
    }
    return parseFloat(result);
}

module.exports = ans;